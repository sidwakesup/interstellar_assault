/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.android.vending.expansion.downloader;

public final class R {
	public static final class drawable {
		public static final int notify_panel_notification_icon_bg = 0x7f02001f;
	}
	public static final class id {
		public static final int appIcon = 0x7f0a0024;
		public static final int description = 0x7f0a002a;
		public static final int notificationLayout = 0x7f0a0023;
		public static final int progress_bar = 0x7f0a0029;
		public static final int progress_bar_frame = 0x7f0a0028;
		public static final int progress_text = 0x7f0a0025;
		public static final int time_remaining = 0x7f0a0027;
		public static final int title = 0x7f0a0026;
	}
	public static final class layout {
		public static final int status_bar_ongoing_event_progress_bar = 0x7f030002;
	}
	public static final class string {
		public static final int kilobytes_per_second = 0x7f07002e;
		public static final int notification_download_complete = 0x7f07001a;
		public static final int notification_download_failed = 0x7f07001b;
		public static final int state_completed = 0x7f070021;
		public static final int state_connecting = 0x7f07001f;
		public static final int state_downloading = 0x7f070020;
		public static final int state_failed = 0x7f07002d;
		public static final int state_failed_cancelled = 0x7f07002c;
		public static final int state_failed_fetching_url = 0x7f07002a;
		public static final int state_failed_sdcard_full = 0x7f07002b;
		public static final int state_failed_unlicensed = 0x7f070029;
		public static final int state_fetching_url = 0x7f07001e;
		public static final int state_idle = 0x7f07001d;
		public static final int state_paused_by_request = 0x7f070024;
		public static final int state_paused_network_setup_failure = 0x7f070023;
		public static final int state_paused_network_unavailable = 0x7f070022;
		public static final int state_paused_roaming = 0x7f070027;
		public static final int state_paused_sdcard_unavailable = 0x7f070028;
		public static final int state_paused_wifi_disabled = 0x7f070026;
		public static final int state_paused_wifi_unavailable = 0x7f070025;
		public static final int state_unknown = 0x7f07001c;
		public static final int time_remaining = 0x7f07002f;
		public static final int time_remaining_notification = 0x7f070030;
	}
	public static final class style {
		public static final int ButtonBackground = 0x7f050008;
		public static final int NotificationText = 0x7f050005;
		public static final int NotificationTextSecondary = 0x7f050009;
		public static final int NotificationTextShadow = 0x7f050006;
		public static final int NotificationTitle = 0x7f050007;
	}
}
